<?php
namespace common\components\monkeytime;

use hiqdev\hiart\AbstractConnection;

class Request extends \hiqdev\hiart\curl\Request
{

    public function send($options = [])
    {
        $response = parent::send($options);

        $headerCount = $response->getHeader('X-Pagination-Total-Count');

        if ( !empty($headerCount) )
            $this->query->count = reset($headerCount);

        return $response;
    }

    public function getBody()
    {
        return !empty($this->body) ? http_build_query($this->body) : $this->body;
    }

}

?>