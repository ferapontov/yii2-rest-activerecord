<?php
namespace common\components\monkeytime;

use yii\base\InvalidConfigException;
use yii\db\QueryInterface;

class ActiveDataProvider extends \hiqdev\hiart\ActiveDataProvider
{
    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        $this->query->all();

        return $this->query->count;
    }

    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = 99999999999;
            if ($pagination->totalCount === 0) {
                return [];
            }
            $this->query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $this->query->addOrderBy($sort->getOrders());
        }

        $pagination->totalCount = $this->getTotalCount();

        return $this->query->all($this->db);
    }
}

?>