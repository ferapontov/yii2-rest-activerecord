<?php
namespace common\components\monkeytime;

use hiqdev\hiart\auto\Request;
use hiqdev\hiart\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

class QueryBuilder extends \hiqdev\hiart\AbstractQueryBuilder
{
    protected $authHeaders = [];

    /**
     * This function is for you to provide your authentication.
     * @param Query $query
     */
    public function buildAuth(Query $query)
    {
        $auth = $this->db->getAuth();
        if (isset($auth['headerToken'])) {
            $this->authHeaders['Authorization'] = 'token ' . $auth['headerToken'];
        }
        if (isset($auth['headerBearer'])) {
            $this->authHeaders['Authorization'] = 'Bearer ' . $auth['headerBearer'];
        }
    }

    public function buildMethod(Query $query)
    {
        static $defaultMethods = [
            'get'       => 'GET',
            'put'       => 'PUT',
            'head'      => 'HEAD',
            'post'      => 'GET',
            'search'    => 'GET',
            'insert'    => 'POST',
            'update'    => 'PUT',
            'delete'    => 'DELETE',
        ];
        return isset($defaultMethods[$query->action]) ? $defaultMethods[$query->action] : 'POST';
    }

    public function buildUri(Query $query)
    {
        if ($query->action == 'update') {
            return Inflector::pluralize($query->from[0]) . '/' . $query->body['id'];
        }

        if ($query->action == 'insert') {
            return Inflector::pluralize($query->from[0]);
        }

        return $query->from;
    }

    public function buildHeaders(Query $query)
    {
        return $this->authHeaders;
    }

    public function buildProtocolVersion(Query $query)
    {
        return null;
    }

    public function buildQueryParams(Query $query)
    {

        $params = [];

        $this->buildWhere($query, $params);
        $this->buildOrderBy($query, $params);
        $this->buildLimit($query, $params);
        $this->buildWith($query, $params);
        $this->buildJoin($query, $params);

        return $params;
    }

    public function buildFormParams(Query $query)
    {
        return [];
    }

    public function buildBody(Query $query)
    {
        if ($query->action == 'update' || $query->action == 'insert') {
            return $query->body;
        }

        return null;
    }

    public function buildLimit(Query $query, &$params) {
        if ($query->limit && $query->limit > 0) {
            $params['per-page'] = $query->limit;

            if ($query->offset && $query->offset > 0) {
                $params['page'] = 1 + $query->offset / $query->limit;
            }
        }
    }

    public function buildOrderBy(Query $query, &$params) {
        if (!empty($query->orderBy))
            $params['order_by'] = json_encode($query->orderBy);
    }

    public function buildWhere(Query $query, &$params) {
        if (!empty($query->where))
            $params['filter'] = json_encode($query->where);
    }

    public function buildWith(Query $query, &$params) {
        if (!empty($query->with))
            $params['expand'] = implode(',', $query->with);
    }

    public function buildJoin(Query $query, &$params) {
        if (!empty($query->joinWith))
            $params['join'] = json_encode(array_column($query->joinWith,0));
    }
}

?>