<?php
namespace common\components\monkeytime;


use hiqdev\hiart\ResponseInterface;

class Connection extends \hiqdev\hiart\rest\Connection
{
    public $services;

    public function getResponseError(ResponseInterface $response)
    {
        $code = $response->getStatusCode();
        if ($code >= 200 && $code < 300) {
            return false;
        }

        if ($code == 100) {
            return false;
        }

        return $response->getReasonPhrase();
    }

}

?>